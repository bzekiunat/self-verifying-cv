/* eslint-env node */
var express = require("express");
var path = require("path");

var app = express();

function backend(port) {
  app.use(express.static(path.join(__dirname, "public")));

  app.listen((port || 2000), function() {
    console.log("server is listening on port:" + (port || 2000));
    console.log("press CTRL + C to quit..");
  });
}

export default backend;
