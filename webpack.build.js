import WebpackDevServer from "webpack-dev-server";
import webpack from "webpack";
import webpackConfig from "./webpack.config";
import gUtil from "gulp-util";

function webpackBuild(inDevMode) {
  var hostConf = {
    host: "localhost",
    port: 3333
  };

  var packageOpts = {
    appMode: inDevMode ? "dev" : "dist"
  };

  var entryObject = inDevMode ? {bundle: ["webpack/hot/dev-server", "babel-polyfill", "./ui/app/index.js"]} : {"./public/js/bundle": ["babel-polyfill", "./ui/app/index.js"]};

  var config = webpackConfig(packageOpts, entryObject);

  gUtil.log("[webpack] started on " + packageOpts.appMode + " mode...");

  if (inDevMode) {
    var onlyDevConfig = config;
    new WebpackDevServer(webpack(onlyDevConfig), {
      publicPath: onlyDevConfig.output.publicPath,
      hot: true,
      historyApiFallback: true,
      stats: "minimal"
    })
      .listen(hostConf.port, hostConf.host, error => {
        if (error) {
          throw new gUtil.PluginError("webpack-dev-server", error);
        }
        gUtil.log("[webpack-dev-server] listening on http://" + hostConf.host + ":" + hostConf.port);
      });
  } else {
    webpack(config, (error, stats) => {
      if (error) {
        throw new gUtil.PluginError("[webpack]", error);
      }

      gUtil.log("[webpack]", stats.toString());
    });
  }
}

export default webpackBuild;
