# Bülent Zeki Unat CV

## Live
http://bzekiunat.com

## What is this project?
This project is proof of that I am capable of anything that is written in my cv.

## How to run
- npm i
- gulp [--port=3000]


## Technologies
- expressJS serves files and responses to get and post requests.
- webpack builds react application
- gulp runs expressJS and webpack
- Redux is used for flowing data

## How to convert to PDF
- Go to localhost:3000/dev
- Click "Print This Page" button at the bottom of page
- Click Destination and chose Save as PDF
- For more details: https://www.labnol.org/software/save-web-page-as-pdf/21153/
 gulp -port=3000
