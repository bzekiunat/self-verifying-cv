import reducer from "./reducer";
import {Provider} from "react-redux";
import {createStore} from "redux";
import CV from "./Cv";

export class Routes extends React.Component {
  render() {
    const store = createStore(reducer);
    return (
      <Provider store={store}>
        <CV />
      </Provider>
    );
  }
}

ReactDOM.render(<Routes/>, document.getElementById("cv-app"));
