import {connect} from "react-redux";
import InfoBox from "../InfoBox";

export class References extends React.Component {
  render() {
    const data = this.props.data;
    return (
      <InfoBox title={"References"}>
        {data.map((reference, index) => (
          <div key={index} className="row">
            <div className="reference-line">
              <span className="reference-name">{reference.get("name")}</span>&nbsp;
              <span className="reference-title">{reference.get("title")}</span>
            </div>
            <div className="reference-line">{reference.get("phone")}</div>
            <div className="reference-line">{reference.get("mail")}</div>
          </div>
        ))}
      </InfoBox>
    );
  }
}

export default connect(state => {
  return {
    data: state.getIn(["cvData", "references"], I.List())
  };
})(References);
