import {connect} from "react-redux";
import InfoBox from "../InfoBox";
import CircularProgressbar from "react-circular-progressbar";

export class Skills extends React.Component {
  render() {
    const data = this.props.data;
    return (
      <InfoBox title={"Skills"}>
        <div className="chart-list">
          {data.map((skill, index) => (
            <div key={index} className="chart-container">
              <div className="skil-name">{skill.get("name")}</div>
              <div className="skil-chart">
                <CircularProgressbar percentage={skill.get("percentage")} />
              </div>
            </div>
          ))}
        </div>
      </InfoBox>
    );
  }
}

export default connect(state => {
  return {
    data: state.getIn(["cvData", "skills"], I.List())
  };
})(Skills);
