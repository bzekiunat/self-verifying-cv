import {connect} from "react-redux";
import InfoBox from "../InfoBox";

export class Experience extends React.Component {
  render() {
    const data = this.props.data;
    return (
      <InfoBox title={"Experience"}>
        {data.map((experience, index) => (
          <div key={index} className="row">
            <div className="header">{experience.get("companyName")}</div>
            <div className="dates">{experience.get("dates")}</div>
            {experience.get("projects").map((project, projectIndex) => (
              <p key={projectIndex}>{project}</p>
            ))}
          </div>
        ))}
      </InfoBox>
    );
  }
}

export default connect(state => {
  return {
    data: state.getIn(["cvData", "experience"], I.List())
  };
})(Experience);
