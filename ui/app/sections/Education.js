import {connect} from "react-redux";
import InfoBox from "../InfoBox";

export class Education extends React.Component {
  render() {
    const data = this.props.data;
    return (
      <InfoBox title={"Education"}>
        {data.map((item, index) => (
          <div key={index} className="row">
            <div className="header">{item.get("department")}</div>
            <div className="dates">{item.get("dates")}</div>
          </div>
        ))}
      </InfoBox>
    );
  }
}

export default connect(state => {
  return {
    data: state.getIn(["cvData", "education"], I.List())
  };
})(Education);
