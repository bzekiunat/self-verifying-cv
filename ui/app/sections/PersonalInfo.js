import {connect} from "react-redux";
import InfoBox from "../InfoBox";

export class PersonalInfo extends React.Component {
  render() {
    const data = this.props.data;
    return (
      <InfoBox title={(
        <div>
          <div className="name">{data.get("name")}</div>
          <div className="title">{data.get("title")}</div>
        </div>
      )}>
        <div className="info-line">{data.get("address")}</div>
        <div className="info-line">{data.get("phone")}</div>
        <div className="info-line">{data.get("email")}</div>
      </InfoBox>
    );
  }
}

export default connect(state => {
  return {
    data: state.getIn(["cvData", "personalInfo"], I.Map())
  };
})(PersonalInfo);
