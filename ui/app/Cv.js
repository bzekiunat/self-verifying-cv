import "./cvStyle.css";
import InfoBox from "./InfoBox";
import PersonalInfo from "./sections/PersonalInfo";
import Skills from "./sections/Skills";
import Education from "./sections/Education";
import Experience from "./sections/Experience";
import References from "./sections/References";
import {connect} from "react-redux";
import request from "../common/request";

export class CV extends React.Component {
  componentDidMount() {
    request.get("/cvdata.json").then(cvData => {
      this.props.setCvData(cvData);
    });
  }

  render() {
    return (
      <div className="app-container">
        <PersonalInfo />
        <Skills />
        <Experience />
        <Education />
        <References />
        <InfoBox title={"Notes"}>
          <div>
            <p>This pdf file is converted from a web page which is implemented by me. <a href={"http://bzekiunat.com/"}>bzekiunat.com/</a></p>
            <p>This web application is an open source project in Bit Bucket. Project link: <a href={"https://bitbucket.org/bzekiunat/self-verifying-cv"}>https://bitbucket.org/bzekiunat/self-verifying-cv</a></p>
            <p>This project implemented with <b>React, Redux, Webpack, Gulp, ExpressJS and Babel</b>, so basically it proves that I am capable of anything that is written in my cv.</p>
          </div>
        </InfoBox>
        <button className="print-btn" onClick={()=> {window.print();}}>Print This Page</button>
      </div>
    );
  }
}

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    setCvData: (data) => {
      dispatch({
        type: "SET_CV_DATA",
        data
      });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CV);
