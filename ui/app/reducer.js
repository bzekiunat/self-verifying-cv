const storeActors = {
  SET_CV_DATA: (state, action) => {
    return state.set("cvData", I.fromJS(action.data));
  }
};

const mainReducer = (state = I.Map(), action) => {
  const actor = storeActors[action.type];
  if (actor) {
    return actor(state, action);
  }
  return state;
};

export default mainReducer;
