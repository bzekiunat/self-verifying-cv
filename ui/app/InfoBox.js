export default class InfoBox extends React.Component {
  render() {
    return (
      <div className="info-container">
        <div className="left-side-container">
          {this.props.title}
        </div>
        <div className="right-side-container">
          {this.props.children}
        </div>
      </div>
    );
  }
}
