import request from "superagent";

const req = {
  get(url) {
    return new Promise((resolve, reject) => {
      request
        .get(url)
        .end((err, res) => {
          if (err || !res.ok) {
            reject({error: err.response.body});
          }
          resolve(res.body);
        });
    });
  },
  post(url, data) {
    return new Promise((resolve, reject) => {
      request
        .post(url)
        .send(data)
        .set("Content-Type", "application/json;charset=UTF-8")
        .end((err, res) => {
          if (err || !res.ok) {
            reject({error: err.response.body});
          }
          resolve(res.body);
        });
    });
  }
};

export default req;
