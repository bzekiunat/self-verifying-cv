/* eslint-env node */
var webpack = require("webpack");
var path = require("path");

module.exports = function config(packageOpts, entryObject) {
  var inDevMode = packageOpts.appMode === "dev";
  var nodeModulesRoot = path.join(__dirname, "node_modules");

  return {
    entry: entryObject,
    module: {
      loaders: [
        {test: /\.js$/, exclude: /node_modules/, loader: inDevMode ? "react-hot!babel" : "babel"},
        {test: /\.css$/, loader: "style-loader!css-loader"},
        {test: /\.woff(\?v=\d+\.\d+\.\d+)?$/, loader: "url?limit=10000&mimetype=application/font-woff"},
        {test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/, loader: "url?limit=10000&mimetype=application/font-woff"},
        {test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: "url?limit=10000&mimetype=application/octet-stream"},
        {test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: "file"},
        {test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, loader: "url?limit=10000&mimetype=image/svg+xml"}
      ]
    },
    noParse: inDevMode ? [
      path.join(nodeModulesRoot, "react"),
      path.join(nodeModulesRoot, "react-dom"),
      path.join(nodeModulesRoot, "immutable")
    ] : [],
    resolve: {
      extensions: ["", ".js", ".jsx"]
    },
    output: {
      path: __dirname,
      publicPath: inDevMode ? "http://localhost:2333/" : "./",
      filename: "[name].js"
    },
    cache: inDevMode,
    devTool: inDevMode ? "source-map" : "cheap-module-source-map",
    debug: inDevMode,
    devServer: inDevMode,
    quiet: true,
    hotComponents: inDevMode,
    plugins: inDevMode ? [
      new webpack.HotModuleReplacementPlugin(),
      new webpack.ProvidePlugin({
        React: "react",
        ReactDOM: "react-dom",
        Immutable: "immutable",
        I: "immutable"
      })
    ] : [
      new webpack.ProvidePlugin({ // !!!!!!!!!!! PROD PLUGINS !!!!!!!!!!
        React: "react",
        ReactDOM: "react-dom",
        Immutable: "immutable",
        I: "immutable"
      }),
      new webpack.DefinePlugin({
        "process.env": {
          NODE_ENV: JSON.stringify("production")
        }
      }),
      new webpack.optimize.UglifyJsPlugin({
        compress: {
          warnings: false
        }
      }),
      new webpack.optimize.DedupePlugin()
    ]
  };
};
