import gulp from "gulp";

/* Webpack Modules & Config */
import webpackBuild from "./webpack.build";
import backend from "./index";

/* Common Utility Plugins */
import yargs from "yargs";
import sequence from "run-sequence";

var argv = yargs.argv;

function Build() {
  gulp.task("webpackBuild", webpackBuild.bind(this, !argv["dist"]));
  gulp.task("backend", backend.bind(this, argv["port"]));
  gulp.task("default", () => {
    sequence(["webpackBuild", "backend"]);
  });
}

Build();
